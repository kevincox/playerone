import gtag from "./ga"

const options = import("./options");

export type Mode = "n" | "g";

export interface Config {
	num: number,
	mode: Mode,
}

let config: Config = {
	num: 1,
	mode: "n",
};

function color(i: number) {
	// Note, this is assumed to be usable in an unquoted HTML attribute.
	return `hsl(${i * 223 % 360},50%,50%)`
}

function shuffle(a: unknown[]) {
	for (let i = a.length; i; ) {
		let r = Math.random() * (i--) |0;
		[a[r], a[i]] = [a[i], a[r]];
	}
}

function updateTouch(element: HTMLElement, event: PointerEvent) {
	element.style.transform = `translate(${event.pageX}px,${event.pageY}px)`;
}

let touches: Record<string, HTMLElement> = {};
let touch_count = 0;
let timeout: number;

let lastt = 0;
let lastx: number, lasty: number;

document.body.addEventListener("pointerdown", e => {
	if (e.target != document.body) return;

	e.preventDefault();

	gtag("event", "touch", {
		event_category: "playerone",
		event_label: "pointerdown",
	});

	if (touch_count == 0) {
		let now = +new Date;

		if (
			now - lastt < 300 &&
			Math.abs(e.pageX - lastx) < 20 &&
			Math.abs(e.pageY - lasty) < 20
		) {
			options.then(options => options.default(config)).then(cfg => {config = cfg});
			return;
		}

		lastx = e.pageX;
		lasty = e.pageY;
		lastt = now;
	}

	if (!(e.pointerId in touches)) {
		touch_count++;
		document.body.insertAdjacentHTML("beforeend",
			`<div class=t style=--c:${color(e.pointerId)}></div>`);
		touches[e.pointerId] = document.body.lastChild as HTMLElement;
		resetTimer();
	}

	updateTouch(touches[e.pointerId], e);
});

document.body.addEventListener("pointermove", e => {
	e.preventDefault();

	let el = touches[e.pointerId];
	if (!el) return;
	updateTouch(el, e);
});

function touchStop(e: PointerEvent) {
	e.preventDefault();

	deleteTouch(e.pointerId);
	resetTimer();

	gtag("event", "touch", {
		event_category: "playerone",
		event_label: e.type,
	});
}
document.body.addEventListener("pointerup", touchStop);
document.body.addEventListener("pointercancel", touchStop);

function deleteTouch(id: number) {
	let el = touches[id];
	if (!el) return;

	delete touches[id];
	touch_count--;

	if (el.classList.contains("d")) {
		return;
	}
	el.classList.add("d");

	el.addEventListener("animationend", e => {
		if (e.pseudoElement == "::after") {
			document.body.removeChild(el);
		}
	});
}

function resetTimer() {
	clearTimeout(timeout);

	let msg = document.getElementById("m")!;

	if (!touch_count) {
		msg.textContent = "touch.";
		return;
	} else if (touch_count <= config.num) {
		msg.textContent = `${+config.num + 1 - touch_count} more`;
		return;
	}

	// @ts-ignore Implicit string/number conversion.
	msg.textContent = 3;
	function tick() {
		// @ts-ignore Implicit string/number conversion.
		if (--msg.textContent) {
			timeout = setTimeout(tick, 1000)
		} else {
			msg.textContent = "";
			selectWinner()
		}
	}
	tick();
}

const modes: Record<Mode, (id: number, i: number) => void> = {
	n: (id, i) => {
		if (i < config.num) {
			if (config.num == 1)
				touches[id].classList.add("w");
		} else {
			deleteTouch(id);
		}
	},
	g: (id, i) => {
		touches[id].style.setProperty("--c", color(i % config.num));
	}
};

function selectWinner() {
	let mode = modes[config.mode];
	let candidates = Object.keys(touches);
	shuffle(candidates);
	candidates.forEach((id, i) => mode(+id, i));

	gtag("event", "complete", {
		event_category: "playerone",
		event_label: config.mode,
		value: candidates.length,
	});
}

resetTimer();
