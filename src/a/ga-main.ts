declare global {
	interface Window { dataLayer: any; }
}

export default function gtag(..._: any) {
	window.dataLayer.push(arguments)
}

// @ts-ignore https://github.com/microsoft/TypeScript/issues/25642
if (window.doNotTrack == 1) {
	window.dataLayer = {push(){}};
} else {
	window.dataLayer = [];

	gtag("js", new Date());
	gtag("config", "UA-45347217-1", {
		link_attribution: true,
		transport_type: 'beacon',
	});

	var s = document.createElement("script");
	s.src = "https://www.googletagmanager.com/gtag/js?id=UA-45347217-1";
	s.async = true;
	document.head.appendChild(s);
}
