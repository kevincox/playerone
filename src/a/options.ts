import html from "bundle-text:./options.slm";
import {Config, Mode} from "./main";

// TODO: Should be a sync import of ./ga but it crashes parcel.
// https://github.com/parcel-bundler/parcel/issues/4145
const ga = import("./ga-main");

document.body.insertAdjacentHTML("beforeend", html);
let overlay = <HTMLDivElement>document.getElementById("options");
let form = <HTMLFormElement>overlay.firstChild;

function update() {
	let mode = form.d.value;
	let num = form.n.value;

	form.querySelector(".numlabel")!.textContent = mode == "g"? "Groups" : "Winners";

	// In groups mode it doesn't make sense to put everyone into one or less group.
	let min = mode == "g"? 2 : 1;
	form.n.min = min;
	if (num < min) num = min;

	form.d.value = mode;
	form.n.value = num;

	return {num, mode}
}

export default async function(config: Config): Promise<Config> {
	return new Promise((resolve, _) => {
		overlay.classList.add("active");

		form.d.value = config.mode;
		form.n.value = config.num;
		update();

		form.onchange = _ => {
			config = update();
		}
		form.onsubmit = event => {
			event.preventDefault();

			resolve(config);

			overlay.classList.remove("active");
		};

		ga.then(ga => ga.default("event", "options", {
			event_category: "playerone",
		}));
	})
}
