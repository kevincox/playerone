# Player One

https://playerone.kevincox.ca

A simple app to select the first player. Use it on the web or install it to your device.

Double tap the screen for the various options.

## Features

- Select any number of fingers.
- Group fingers into any number of groups.
- Finger count is only limited by your device.
- Works completely offline.
- Fast to load and lightweight (The base functionality is <2KB to download).