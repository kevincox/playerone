{
	nixpkgs ? import <nixpkgs> {},
	kevincox-web-compiler ? builtins.storePath (nixpkgs.lib.fileContents (builtins.fetchurl  "https://kevincox.gitlab.io/kevincox-web-compiler/bin.nixpath")),
	# kevincox-web-compiler ? (import ../kevincox-web-compiler {}).bin,
	napalm ? nixpkgs.pkgs.callPackage (
		fetchTarball "https://github.com/nix-community/napalm/archive/master.tar.gz"
	) {},
}: with nixpkgs; let
	render-svg = svg: {width, format ? "svg"}: let
		basename = lib.removeSuffix ".svg" (baseNameOf svg);
		width-str = toString width;
	in pkgs.runCommand "${basename}-${width-str}.png" {} ''
		${pkgs.librsvg}/bin/rsvg-convert -w ${width-str} -a ${svg} >$out
	'';
in rec {
	npm = napalm.buildPackage
		(pkgs.nix-gitignore.gitignoreSource [
			"*"
			"!package.json"
			"!package-lock.json"
		] ./.)
		{
			buildInputs = with pkgs; [ python3 ];
		};

	unoptimized = stdenv.mkDerivation {
		name = "playerone-unoptimized";

		src = (pkgs.nix-gitignore.gitignoreSource [] ./.);

		buildPhase = ''
			ln -s ${npm}/_napalm-install/node_modules .
			node_modules/.bin/parcel build --dist-dir=$out --no-optimize --no-source-maps -- src/*.slm src/service-worker.ts

			# Parcel converts Web Manifests to JS 🤦
			sed -i -e 's/manifest\.js/manifest.json/g' $out/*.html
			rm -v $out/manifest.js
			cp -v src/manifest.json $out/

			cp -rv src/.well-known $out

			mkdir $out/a
			cp -v src/a/*.{png,svg} $out/a

			cp -v ${render-svg ./src/a/logo.svg {width=192;}} $out/a/logo-192.png
			cp -v ${render-svg ./src/a/logo.svg {width=512;}} $out/a/logo-512.png
		'';

		installPhase = ''
			cp src/ping $out/
		'';
	};

	www = pkgs.runCommandLocal "playerone-www" {} ''
		${kevincox-web-compiler}/bin/kevincox-web-compiler \
			--no-analytics \
			--asset-mode=hash \
			${unoptimized} $out
	'';
}
