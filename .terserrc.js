module.exports = {
	compress: {
		ecma: 2018,
		keep_fargs: false,
		passes: 4,
		unsafe: true,
		warnings: true,
	},
};
